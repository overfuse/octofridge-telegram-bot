# coding=utf-8

import telebot
import cart
import re
import dobsky
import pymorphy2

from config import TOKEN
bot = telebot.TeleBot(TOKEN)
_state = {}

morph = pymorphy2.MorphAnalyzer()


def normal_form(w):
    p = morph.parse(w)
    if p:
        return p[0].normal_form
    return w


def get_command_body(text):
    return re.sub('^/[a-z_]+[ ]?(.*)$', '\\1', text)


@bot.message_handler(commands=['help'])
def help(message):
    bot.send_message(
        message.chat.id,
        u"""/add - Добавить в корзину
/list - Содержимое корзины
/define - Уточнить товар
/checkout - Подтвердить заказ
/remove - Удалить из корзины
/clear - Очистить корзину
/cancel - Отмена
""", reply_markup=_start_markup())


@bot.message_handler(commands=['start'])
def start(message):
    bot.send_message(message.chat.id, dobsky.get_hello(), reply_markup=_start_markup())


def _start_markup():
    markup = telebot.types.ReplyKeyboardMarkup(row_width=2, selective=False)
    markup.add('/add', '/list', '/define', '/checkout', '/remove', '/clear')
    return markup


def _hide_markup():
    return telebot.types.ReplyKeyboardHide(selective=False)


@bot.message_handler(commands=['add'])
def add_item(message):
    text = get_command_body(message.text)
    if text:
        title = normal_form(text)
        item = cart.add_item(title, message.chat.id)
        if item:
            if not _start_define(message, title, item=item):
                bot.send_message(message.chat.id, u"Добавил %s" % title, reply_markup=_start_markup())
        else:
            bot.send_message(message.chat.id, u"Извините, продажа %s запрещена на территории вашей страны" % title, reply_markup=_start_markup())
    else:
        msg = bot.send_message(message.chat.id, u'Напишите название', reply_markup=_hide_markup())
        bot.register_next_step_handler(msg, add_item_handler)


def add_item_handler(message):
    if 'text' not in message.content_type:
        return
    title = normal_form(message.text)
    if title:
        item = cart.add_item(title, message.chat.id)
        if item:
            if not _start_define(message, title):
                bot.send_message(message.chat.id, u"Добавил %s" % title, reply_markup=_start_markup())
        else:
            bot.send_message(message.chat.id, u"Извините, продажа %s запрещена" % title, reply_markup=_start_markup())


@bot.message_handler(commands=['list'])
def get_items(message):
    items = cart.get_items(message.chat.id)
    if items:
        text = "\n".join(['%s. %s' % (i + 1, cart.format_item(x)) for i, x in enumerate(items)])
        total_price = cart.total_price(items)
        if total_price:
            text += u"\n---\nИтого: {0:g} руб".format(cart.total_price(items))
        bot.send_message(message.chat.id, text, reply_markup=_start_markup())
    else:
        bot.send_message(message.chat.id, u"Корзина пуста", reply_markup=_start_markup())


@bot.message_handler(commands=['remove'])
def remove_item(message):
    items = cart.get_items(message.chat.id)
    if items:
        markup = telebot.types.ReplyKeyboardMarkup(row_width=2, selective=False)
        markup.add(*[x['title'] for x in items])
        msg = bot.send_message(message.chat.id, u'Что удалить?', reply_markup=markup)
        bot.register_next_step_handler(msg, remove_item_handler)
    else:
        bot.send_message(message.chat.id, u"Корзина пуста")


def remove_item_handler(message):
    if 'text' not in message.content_type:
        return
    title = get_command_body(message.text)
    item = cart.get_by_title(title, message.chat.id)
    if item:
        cart.remove_item(item['id'], message.chat.id)
        markup = telebot.types.ReplyKeyboardHide(selective=False)
        bot.send_message(message.chat.id, u"Удалил %s" % item['title'], reply_markup=markup)
    else:
        bot.send_message(message.chat.id, u"%s нет в корзине" % title)


@bot.message_handler(commands=['clear'])
def clear_items(message):
    markup = telebot.types.ReplyKeyboardMarkup(row_width=2, selective=False)
    markup.add(u'Да', u'Нет')
    msg = bot.send_message(message.chat.id, u'Очистить корзину?', reply_markup=markup)
    bot.register_next_step_handler(msg, clear_handler)


def clear_handler(message):
    if 'text' in message.content_type:
        text = message.text
    else:
        text = ''
    if text == u'Да':
        items = cart.get_items(message.chat.id)
        for x in items:
            cart.remove_item(x['id'], message.chat.id)
        bot.send_message(message.chat.id, u"Удалил все из корзины", reply_markup=_start_markup())
    else:
        bot.send_message(message.chat.id, u"ОК", reply_markup=_start_markup())


@bot.message_handler(commands=['define'])
def def_item(message):
    title = get_command_body(message.text)
    if title:
        _start_define(message, title, True)
    else:
        items = cart.get_items(message.chat.id)
        if items:
            markup = telebot.types.ReplyKeyboardMarkup(row_width=2, selective=False)
            markup.add(*[x['title'] for x in items])
            msg = bot.send_message(message.chat.id, u'Какой товар уточнить?', reply_markup=markup)
            bot.register_next_step_handler(msg, def_handler)
        else:
            bot.send_message(message.chat.id, u"Корзина пуста")


def def_handler(message):
    title = message.text
    _start_define(message, title, True)


def _start_define(message, title, force=False, item=None):
    item = item or cart.get_by_title(title, message.chat.id)
    if item:
        d = cart.def_item(item['id'], message.chat.id)
        if d and (d['answers'] or force):
            _state['item_id'] = item['id']
            _state['chat_id'] = message.chat.id
            m = _define_message(d)
            msg = bot.send_message(message.chat.id, **m)
            bot.register_next_step_handler(msg, define_item_handler)
            return True
    elif force:
        bot.send_message(message.chat.id, u"%s нет в корзине" % title)


def _define_message(d):
    text = d['question']
    if d['answers']:
        markup = telebot.types.ReplyKeyboardMarkup(row_width=2, selective=False)
        markup.add(*d['answers'])
    else:
        markup = _hide_markup()
    return {'text': text, 'reply_markup': markup}


def define_item_handler(message):
    if 'text' not in message.content_type:
        return
    d = cart.def_item(_state['item_id'], message.chat.id, message.text)
    if d:
        m = _define_message(d)
        msg = bot.send_message(message.chat.id, **m)
        if d['finished']:
            item = cart.get_item(_state['item_id'], message.chat.id)
            bot.send_message(message.chat.id, cart.format_item(item), reply_markup=_start_markup())
            _finish_state()
        elif _state['chat_id']:
            bot.register_next_step_handler(msg, define_item_handler)


def _finish_state():
    _state['chat_id'] = None
    _state['checkout'] = None


@bot.message_handler(commands=['cancel'])
def cancel(message):
    _finish_state()
    bot.send_message(message.chat.id, u'Отмена', reply_markup=_start_markup())


@bot.message_handler(regexp=u'Все')
@bot.message_handler(commands=['checkout'])
def confirm(message):
    undefined = cart.get_items_undefined(message.chat.id)
    if not len(undefined):
        markup = telebot.types.ReplyKeyboardMarkup(row_width=2, selective=False)
        markup.add(u'Да', u'Нет')
        msg = bot.send_message(message.chat.id, u'Оформить заказ?', reply_markup=markup)
        bot.register_next_step_handler(msg, confirm_handler)
    else:
        markup = telebot.types.ReplyKeyboardMarkup(row_width=2, selective=False)
        markup.add(*[x['title'] for x in undefined])
        msg = bot.send_message(message.chat.id, u"Нужно уточнить товары:", reply_markup=markup)
        bot.register_next_step_handler(msg, def_handler)


def confirm_handler(message):
    if 'text' in message.content_type:
        text = message.text
    else:
        text = ''
    if text == u'Да':
        cart.checkout(message.chat.id)
        bot.send_message(message.chat.id, u'Переходим к оплате', reply_markup=_hide_markup())
    else:
        bot.send_message(message.chat.id, u'Что-нибудь еще?', reply_markup=_start_markup())


if __name__ == '__main__':
    bot.polling()
