# coding=utf-8

import telegram
from flask import Flask, request

app = Flask(__name__)
app.config.from_pyfile('config.py')

bot = telegram.Bot(token=app.config['TOKEN'])


@app.route('/bot/webhook', methods=['POST'])
def webhook_handler():
    if request.method == "POST":
        data = request.get_json(force=True)
        update = telegram.Update.de_json(data)

        chat_id = update.message.chat.id
        text = update.message.text

        if text:
            bot.sendMessage(chat_id=chat_id, text=text)
        else:
            print data

    return 'OK'


@app.route('/bot/send_message', methods=['GET'])
def send_message():
    chat_id = request.args['chat_id']
    text = request.args['text']
    bot.sendMessage(chat_id=chat_id, text=text)
    return text


def add_item(bot, update):
    bot.sendMessage(update.message.chat_id, text=update.message.text)


def set_webhook():
    s = bot.setWebhook(app.config['BASE_URL'] + '/bot/webhook')
    if s:
        return "webhook setup ok"
    else:
        return "webhook setup failed"


@app.route('/bot')
def index():
    return set_webhook()


if __name__ == "__main__":
    app.run()
