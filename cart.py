# coding=utf-8
import requests
import json
import requests.packages.urllib3
requests.packages.urllib3.disable_warnings()

base_url = 'https://fridge.octoberry.ru/cart'


def is_json(s):
    return s[0] in ('{', '[')


def _get(uri, chat_id, **kwargs):
    print 'GET', base_url + uri, chat_id
    r = requests.get(base_url + uri, headers={'X-Fridge-Chat-Id': chat_id}, **kwargs)
    print r.text
    return _response(r)


def _post(uri, data, chat_id):
    print 'POST', base_url + uri, chat_id
    r = requests.post(base_url + uri, json.dumps(data), headers={'X-Fridge-Chat-Id': chat_id})
    print r.text
    return _response(r)


def _response(r):
    if r.status_code < 400:
        if is_json(r.text):
            return json.loads(r.text)
        return r.text


def add_item(title, chat_id):
    return _post('/items', {'title': title}, chat_id)


def get_item(item_id, chat_id):
    return _get('/item/'+item_id, chat_id)


def get_items(chat_id):
    items = _get('/items', chat_id) or []
    return sorted(items, key=lambda x: x['id'])


def get_by_title(title, chat_id):
    items = get_items(chat_id)
    if title.isdigit():
        idx = int(title) - 1
        print idx, len(items)
        if 0 <= idx < len(items):
            return items[idx]
    for x in items:
        if title == x['title']:
            return x


def format_item(item):
    s = [item['title']]
    if item['shop_name']:
        if item['shop_name'].lower().startswith(item['title'].lower()):
            s = [item['shop_name']]
        else:
            s.append(item['shop_name'])
    if item['price']:
        s.append(u'цена {0:g} руб'.format(item['price']))
    if item['count'] and item['count'] > 1:
        s.append(u'x %s шт' % item['count'])
    return ' '.join(s)


def total_price(items):
    return sum([x['price'] and x['count'] and x['price'] * x['count'] or 0 for x in items])


def remove_item(item_id, chat_id):
    requests.delete(base_url + '/item/' + item_id, headers={'X-Fridge-Chat-Id': chat_id})


def def_item(item_id, chat_id, title=''):
    params = title and {'title': title} or None
    return _get('/item/%s/define' % item_id, chat_id, params=params)


def format_define(d):
    s = d['question']
    if d['answers']:
        s += ' (%s)' % ', '.join(d['answers'])
    return s


def get_items_undefined(chat_id):
    items = get_items(chat_id)
    return filter(lambda x: not x['price'], items)


def checkout(chat_id):
    _post('', {}, chat_id)
